﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace Capa_Datos
{
    public class DACliente
    {

       private SqlConnection connection = new SqlConnection(@"Data Source=segundo150\segundo150;Initial Catalog=DAM_InakiInda;Integrated Security=True");

        public int AltaCliente(EntidadCliente c)
        {
            const string cmdText = "INSERT INTO Panaderia.Clientes (CodCliente, Tipo,Nombre,Apellido1,Apellido2,CP,Direccion,Telefono) VALUES(@CodCliente,@tipo,@nombre,@apellido1,@apellido2,@cp,@direccion,@telefono)";

            c.CodCliente = SiguienteCliente();
            using (SqlCommand cmd = new SqlCommand(cmdText, connection))
            {
                cmd.Parameters.AddWithValue("@CodCliente", c.CodCliente);
                cmd.Parameters.AddWithValue("@tipo", c.Tipo);
                cmd.Parameters.AddWithValue("@nombre", c.Nombre);
                cmd.Parameters.AddWithValue("@apellido1", c.Apellido1);
                cmd.Parameters.AddWithValue("@apellido2", c.Apellido2);
                cmd.Parameters.AddWithValue("@cp", c.Cp);
                cmd.Parameters.AddWithValue("@direccion", c.Direccion);
                cmd.Parameters.AddWithValue("@telefono", c.Telefono);

                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;
            }

        }
        public string SiguienteCliente()
        {
            const string cmdtext = "SELECT MAX(CAST(CodCliente AS INT))+1 FROM Panaderia.Clientes";
            string id;
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                connection.Open();
                id = Convert.ToString(cmd.ExecuteScalar());
                connection.Close();
            }
            return id;
        }
        public int BorrarCliente(string c)
        {
            const string cmdtext = "DELETE FROM Panaderia.Clientes WHERE CodCliente=@Cod";

            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                cmd.Parameters.AddWithValue("@Cod", c);
                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;

            }
        }
        public int ModificarCliente(EntidadCliente ec)
        {
            const string cmdtext = "UPDATE Panaderia.Clientes SET Tipo=@tipo,nombre=@nombre,Apellido1=@apellido1,Apellido2=@apellido2,CP=@cp,Direccion=@direccion,Telefono=@telefono WHERE CodCliente=@codcliente";
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                cmd.Parameters.AddWithValue("@codcliente", ec.CodCliente);
                cmd.Parameters.AddWithValue("@tipo", ec.Tipo);
                cmd.Parameters.AddWithValue("@nombre", ec.Nombre);
                cmd.Parameters.AddWithValue("@apellido1", ec.Apellido1);
                cmd.Parameters.AddWithValue("@apellido2", ec.Apellido2);
                cmd.Parameters.AddWithValue("@cp", ec.Cp);
                cmd.Parameters.AddWithValue("@direccion", ec.Direccion);
                cmd.Parameters.AddWithValue("@telefono", ec.Telefono);
                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;

            }
        }
    }
}
