﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Windows.Forms;

namespace Capa_Datos
{
    public class DAReserva
    {
       
        //Conexion SEIM
        private SqlConnection connection = new SqlConnection(@"Data Source=segundo150\segundo150;Initial Catalog=DAM_InakiInda;Integrated Security=True");
        BindingSource  = new BindingSource();
        //Conexion portatil
        // private SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-FSOQTEV;Initial Catalog=DAM_InakiInda;Integrated Security=True");

        public int AltaReserva(EntidadReserva r)
        {
           
            const string cmdText = "INSERT INTO Panaderia.Reservas (IdReserva,CodCliente,fecha,Prioridad,Precio,Realizada) VALUES(@IdReserva,@CodCliente,@fecha,@prioridad,@precio,@realizada)";

            r.IdReserva = SiguienteReserva();
            using (SqlCommand cmd = new SqlCommand(cmdText, connection))
            {
                cmd.Parameters.AddWithValue("@IdReserva", r.IdReserva);
                cmd.Parameters.AddWithValue("@CodCliente", r.CodCliente);
                cmd.Parameters.AddWithValue("@fecha", r.Fecha);
                cmd.Parameters.AddWithValue("@prioridad", r.Priorida);
                cmd.Parameters.AddWithValue("@precio", r.Precio);
                cmd.Parameters.AddWithValue("@realizada", r.Realizada);


                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;
            }

        }
        public int AltaDetalleReserva(EntidadDetallesReserva r)
        {
            const string cmdText = "INSERT INTO Panaderia.DetalleReservas (IdReserva,CodCliente,NumLinea,CodProducto,Cantidad) VALUES(@IdReserva,@CodCliente,@Numlinea,@Codproducto,@cantidad)";
           
            r.IdReserva = SiguienteDetalleReserva();
            r.Numlinea = SiguienteNumLinea(r.IdReserva);
            using (SqlCommand cmd = new SqlCommand(cmdText, connection))
            {
                cmd.Parameters.AddWithValue("@IdReserva", r.IdReserva);
                cmd.Parameters.AddWithValue("@CodCliente", r.CodCliente);
                cmd.Parameters.AddWithValue("@Numlinea", r.Numlinea);
                cmd.Parameters.AddWithValue("@Codproducto", r.CodProducto);
                cmd.Parameters.AddWithValue("@cantidad", r.Cantidad);


                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;
            }
        }

        private string SiguienteDetalleReserva()
        {
            const string cmdtext = "SELECT MAX(CAST(IdReserva AS INT))+1 FROM Panaderia.DetalleReservas";
            string id;
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                connection.Open();
                id = Convert.ToString(cmd.ExecuteScalar());
                connection.Close();
            }
            return id;
        }

        public void ActualizarGrid(string id)
        {
            string cmdtxt = "SELECT * FROM Panaderia.Reservas WHERE IdReserva=@idres";
            using (SqlCommand cmd = new SqlCommand(cmdtxt, connection))
            {
                cmd.Parameters.AddWithValue("@idres", id);
                connection.Open();
                cmd.ExecuteNonQuery();
                connection.Close();
            }

        }

        private string SiguienteReserva()
        {
            const string cmdtext = "SELECT MAX(CAST(IdReserva AS INT))+1 FROM Panaderia.Reservas";
            string id;
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                connection.Open();
                id = Convert.ToString(cmd.ExecuteScalar());
                connection.Close();
            }
            return id;
        }
    
        public string SiguienteNumLinea(string idreserva)
        {
            string cmdtext = "SELECT MAX(CAST(Numlinea AS INT))+1 FROM Panaderia.DetalleReservas where IdReserva=@idreserva";
            string id;
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                cmd.Parameters.AddWithValue("@idreserva", idreserva);
                connection.Open();
                id = Convert.ToString(cmd.ExecuteScalar());
                connection.Close();
            }
            return id;
        }

        public int ModificarReserva(EntidadReserva r)
        {
            string cmdtext = "UPDATE Panaderia.Reservas SET CodCliente=@CodCliente, fecha=@fecha,Prioridad=@prioridad,Precio=@precio WHERE IdReserva=@idreserva";
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                cmd.Parameters.AddWithValue("@CodCliente", r.CodCliente);
                cmd.Parameters.AddWithValue("@fecha", r.Fecha);
                cmd.Parameters.AddWithValue("@prioridad", r.Priorida);
                cmd.Parameters.AddWithValue("@precio", r.Precio);
                cmd.Parameters.AddWithValue("@idreserva", r.IdReserva);


                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;
            }
        }
        public int ModificarDetalleReserva(EntidadDetallesReserva r)
        {

            string cmdtext = "UPDATE Panaderia.DetalleReservas SET CodProducto=@codprod,Cantidad=@cant WHERE IdReserva=@idreserva and CodCliente=@codcliente and Numlinea=@numlinea";
            using (SqlCommand cmd = new SqlCommand(cmdtext, connection))
            {
                cmd.Parameters.AddWithValue("@CodCliente", r.CodCliente);
                cmd.Parameters.AddWithValue("@idreserva", r.IdReserva);
                cmd.Parameters.AddWithValue("@numlinea", r.Numlinea);
                cmd.Parameters.AddWithValue("@codprod", r.CodProducto);
                cmd.Parameters.AddWithValue("@cant", r.Cantidad);


                connection.Open();
                int resultado = cmd.ExecuteNonQuery();
                connection.Close();
                return resultado;
            }
        }
        public void Mostrarcuentascb()
        {
            connection.Open();
            bscuenta.DataSource = cmdCuentas.ExecuteReader();
            cbcuentas.DataSource = bscuenta;
            cbcuentas.DisplayMember = "Descripcion";
            cbcuentas.ValueMember = "CodCuenta";
            conexion.Close();
        }
    }
}
