﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadCliente
    {
        //private String codCliente;
        //private Boolean tipo;
        //private String nombre;
        //private String apellido1;
        //private String apellido2;
        //private String cp;
        //private String direccion;
        //private String telefono;

        public string CodCliente { get; set; }
        public bool Tipo { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Cp { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
    }
    public class EntidadReserva
    {
        //private string IdReserva;
        //private string CodCliente;
        //private DateTime Fecha;
        //private int Prioridad;
        //private int Precio;
        //private bool Realizada;

        public string IdReserva { get; set; }
        public string CodCliente { get; set; }
        public DateTime Fecha { get; set; }
        public int Priorida { get; set; }
        public float Precio { get ; set; }
        public bool Realizada { get; set ; }
    }
    public class EntidadDetallesReserva
    {
        //private string IdReserva;
        //private string CodCliente;
        //private string Numlinea;
        //private string CodProducto;
        //private int Cantidad;

        public string IdReserva{ get; set; }
        public string CodCliente { get; set; }
        public string Numlinea { get; set; }
        public string CodProducto { get; set; }
        public int Cantidad { get; set; }
    }
}
