﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Capa_Datos;

namespace Capa_Negocio
{
   public class BSCliente
    {
        private DACliente nuevocliente = new DACliente();
        public int AltaCliente(EntidadCliente cliente)
        {
            
            int resutlado=nuevocliente.AltaCliente(cliente);
            return resutlado;
        }
       public int BorrarCliente(string c)
        {
            int resultado = nuevocliente.BorrarCliente(c);
            return resultado;
        }
        public int ModificarCliente(EntidadCliente ec)
        {
            int resultado = nuevocliente.ModificarCliente(ec);
            return resultado;
        }
    }
}
