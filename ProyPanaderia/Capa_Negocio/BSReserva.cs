﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Capa_Datos;

namespace Capa_Negocio
{
   public class BSReserva
    {
        private DAReserva nuevareserva = new DAReserva();
        public int AltaReserva (EntidadReserva r)
        {

            int resutlado = nuevareserva.AltaReserva(r);
            return resutlado;
        }
         public int AltaDetallesReserva(EntidadDetallesReserva r)
        {
            int resultado = nuevareserva.AltaDetalleReserva(r);
            return resultado;
        }
      public int ModificarReserva(EntidadReserva r)
        {
            int res = nuevareserva.ModificarReserva(r);
            return res;
        }
        public void ActualizarGrid(string id)
        {
            nuevareserva.ActualizarGrid(id);
        }
        public string SiguienteNumLinea(string idreserva)
        {
            string res;
           res= nuevareserva.SiguienteNumLinea(idreserva);
            return res;
        }
        public int ModificarDetalleReserva(EntidadDetallesReserva r)
        {
           int resultado = nuevareserva.ModificarDetalleReserva(r);
            return resultado;
        }
    }

}
