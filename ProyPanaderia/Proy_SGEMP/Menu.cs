﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proy_SGEMP
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void gestiónDeReservasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GReservas f1 = new GReservas();
            f1.Show();
        }

        private void agregarClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ACliente f1 = new ACliente();
            f1.Show();
        }

        private void modificarClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MClientes f1 = new MClientes();
            f1.Show();
        }
    }
}
