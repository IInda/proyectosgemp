﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Capa_Negocio;
using Entidades;

namespace Capa_Presentacion
{
    public partial class Form_Principal : Form
    {
        private BSCliente blCliente = new BSCliente();
        private BSReserva bsReserva = new BSReserva();
        public Form_Principal()
        {
            InitializeComponent();
        }

        private void Form_Principal_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dS_Productos.Productos' Puede moverla o quitarla según sea necesario.
            this.productosTableAdapter.Fill(this.dS_Productos.Productos);
            // TODO: esta línea de código carga datos en la tabla 'dS_DetalleReservas.DetalleReservas' Puede moverla o quitarla según sea necesario.
            this.detalleReservasTableAdapter.Fill(this.dS_DetalleReservas.DetalleReservas);
            // TODO: esta línea de código carga datos en la tabla 'dS_Reservas.Reservas' Puede moverla o quitarla según sea necesario.
            this.reservasTableAdapter.Fill(this.dS_Reservas.Reservas);
            // TODO: esta línea de código carga datos en la tabla 'dS_Clientes.Clientes' Puede moverla o quitarla según sea necesario.
            this.clientesTableAdapter.Fill(this.dS_Clientes.Clientes);


            public void Mostrarcuentascb()
            {
                conexion.Open();
                bscuenta.DataSource = cmdCuentas.ExecuteReader();
                cbcuentas.DataSource = bscuenta;
                cbcuentas.DisplayMember = "Descripcion";
                cbcuentas.ValueMember = "CodCuenta";
                conexion.Close();
            }


        }

        /*CLIENTES*/
        /*CLIENTE AÑADIR*/
        private void btanadir_Click(object sender, EventArgs e)
        {
            bool t;
            int resultado;
            if (cbempresa.Checked)
            {
                t = true;
            }
            else t = false;
            EntidadCliente ec = new EntidadCliente()
            {
                Nombre = txtnombre.Text,
                Apellido1 = txtapellido.Text,
                Apellido2 = txtapellido2.Text,
                Tipo = t,
                Direccion = txtdireccion.Text,
                Cp = txtcp.Text,
                Telefono = txttelefono.Text
            };
            resultado = blCliente.AltaCliente(ec);
            txtnombre.Clear();
            txtapellido.Clear();
            txtapellido2.Clear();
            txtdireccion.Clear();
            txtcp.Clear();
            txttelefono.Clear();
            if (resultado != 0)
            {
                MessageBox.Show("Registro añadido con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.clientesTableAdapter.Fill(this.dS_Clientes.Clientes);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView gridView = (sender as DataGridView);
            DataGridViewRow row = gridView.Rows[e.RowIndex];

            string CodCliente = row.Cells["CodCliente"].Value.ToString();
            string Nombre = row.Cells["Nombre"].Value.ToString();
            bool tipo = Convert.ToBoolean(row.Cells["Tipo"].Value);
            string Ape1 = row.Cells["Apellido1"].Value.ToString();
            string Ape2 = row.Cells["Apellido2"].Value.ToString();
            string Cp = row.Cells["CP"].Value.ToString();
            string Direccion = row.Cells["Direccion"].Value.ToString();
            string Telefono = row.Cells["Telefono"].Value.ToString();

            txtCodClienteborrar.Text = CodCliente;
            txtnombreborrar.Text = Nombre;
            cbtipoborrar.Checked = tipo;
            txtape1borrar.Text = Ape1;
            txtape2borrar.Text = Ape2;
            txtcpborrar.Text = Cp;
            txtdireccionborrar.Text = Direccion;
            txttelefonoborrar.Text = Telefono;
        }
        /*CLIENTE BORRAR*/
        private void bt_borrar_Click(object sender, EventArgs e)
        {
            int resultado;

            string CodCliente = txtCodClienteborrar.Text;
            resultado = blCliente.BorrarCliente(CodCliente);
            if (resultado != 0)
            {
                MessageBox.Show("Registro borrado con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.clientesTableAdapter.Fill(this.dS_Clientes.Clientes);
        }
        /*CLIENTE MODIFICAR*/
        private void Bt_Modificar_Click(object sender, EventArgs e)
        {
            int resultado;
            bool t;
            if (cbtipoborrar.Checked)
            {
                t = true;
            }
            else t = false;


            EntidadCliente ec = new EntidadCliente()
            {
                CodCliente = txtCodClienteborrar.Text,
                Nombre = txtnombreborrar.Text,
                Apellido1 = txtape1borrar.Text,
                Apellido2 = txtape2borrar.Text,
                Tipo = t,
                Direccion = txtdireccionborrar.Text,
                Cp = txtcpborrar.Text,
                Telefono = txttelefonoborrar.Text
            };
            resultado = blCliente.ModificarCliente(ec);

            if (resultado != 0)
            {
                MessageBox.Show("Registro modificado con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.clientesTableAdapter.Fill(this.dS_Clientes.Clientes);
        }

        /*RESERVAS*/


        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int indice = cbprioridad.SelectedIndex;
            foreach (int i in cbprioridad.CheckedIndices)
            {
                if (i != indice)
                {
                    cbprioridad.SetItemChecked(i, false);
                }
            }
        }

        private void bt_anadir_reserva_click(object sender, EventArgs e)
        {

            bool res;
            int resultado;
            if (rbsi.Checked)
            {
                res = true;
            }
            else res = false;


            EntidadReserva er = new EntidadReserva()
            {
                CodCliente = txtreservacodcliente.Text,
                Fecha = dtmodfecha.Value,
                Precio = int.Parse(txtprecio.Text.Replace("€", "")),
                Realizada = res,
                Priorida = int.Parse(cbprioridad.Text)
            };
            resultado = bsReserva.AltaReserva(er);

            if (resultado != 0)
            {
                MessageBox.Show("Registro añadido con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.reservasTableAdapter.Fill(this.dS_Reservas.Reservas);


            tabControl1.SelectedIndex = (tabControl1.SelectedIndex + 1);
            lbncliente.Text = er.CodCliente;
            lbnreserva.Text = er.IdReserva;
        }


        private void btBuscarReserva_Click(object sender, EventArgs e)
        {
            string IdReserva = txtidmodre.Text;

            bsReserva.ActualizarGrid(IdReserva);

        }

        private void dataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView gridView = (sender as DataGridView);
            DataGridViewRow row = gridView.Rows[e.RowIndex];

            string IdReserva = row.Cells["IdReserva"].Value.ToString();
            string CodCliente = row.Cells["CodClienter"].Value.ToString();
            DateTime fecha = Convert.ToDateTime(row.Cells["fecha"].Value);
            int prioridad = Convert.ToInt16(row.Cells["Prioridad"].Value);
            string precio = (row.Cells["Precio"].Value.ToString());
            bool rea = Convert.ToBoolean(row.Cells["Realizada"].Value);

            txtidmodre.Text = IdReserva;
            txtcodclimodre.Text = CodCliente;
            dpmodre.Value = fecha;
            cbmodre.SelectedIndex = prioridad-1;
            txtpreciomodre.Text =precio;
            rbmodre.Checked = rea;

        }


        private void btn_agregardr_Click(object sender, EventArgs e)
        {
            int resultado;
            EntidadDetallesReserva er = new EntidadDetallesReserva()
            {
                IdReserva = lbnreserva.Text,
                CodCliente = lbncliente.Text,
                Cantidad = int.Parse(txtcantidaddr.Text),
                CodProducto = cbproductosdr.Text,
            };
            resultado = bsReserva.AltaDetallesReserva(er);
            if (resultado != 0)
            {
                MessageBox.Show("Registro añadido con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            txtcantidaddr.Clear();
                
        }

        private void btactumodre_Click(object sender, EventArgs e)
        {
            bool res;
            int resultado;
            if (rbmodre.Checked)
            {
                res = true;
            }
            else res = false;


            EntidadReserva er = new EntidadReserva()
            {
                IdReserva = txtidmodre.Text,
                CodCliente = txtcodclimodre.Text,
                Fecha = dpmodre.Value,
                Precio = float.Parse(txtpreciomodre.Text.Replace("€", "")),
                Realizada = res,
                Priorida = int.Parse(cbmodre.Text)
            };


            resultado = bsReserva.ModificarReserva(er);

            if (resultado != 0)
            {
                MessageBox.Show("Registro modificado con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.reservasTableAdapter.Fill(this.dS_Reservas.Reservas);


        }
        //actualizar Reserva y continuar a detalle reserva
        private void btcontDR_Click(object sender, EventArgs e)
        {
            bool res;
            int resultado;
            if (rbmodre.Checked)
            {
                res = true;
            }
            else res = false;


            EntidadReserva er = new EntidadReserva()
            {
                IdReserva = txtidmodre.Text,
                CodCliente = txtcodclimodre.Text,
                Fecha = dpmodre.Value,
                Precio = float.Parse(txtpreciomodre.Text.Replace("€","")),
                Realizada = res,
                Priorida = int.Parse(cbmodre.Text)
            };
           

            resultado = bsReserva.ModificarReserva(er);

            if (resultado != 0)
            {
                MessageBox.Show("Registro modificado con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.reservasTableAdapter.Fill(this.dS_Reservas.Reservas);


            tabControl1.SelectedIndex = (tabControl1.SelectedIndex + 1);
            lblnclientedr.Text = er.CodCliente;
            lblnreservadr.Text = er.IdReserva;
        }

        private void btactumoddr_Click(object sender, EventArgs e)
        {
            //bsReserva.SiguienteNumLinea(id);
            //cblineamod.Update();
        }

        private void btmoddr_Click(object sender, EventArgs e)
        {
            int resultado;
            EntidadDetallesReserva ed = new EntidadDetallesReserva()
            {
                Numlinea = cblineamod.Text,
                IdReserva = lblnreservadr.Text,
                CodCliente = lblnclientedr.Text,
                CodProducto = cbproductomod.Text,
                Cantidad = int.Parse(txtcantidadmod.Text)

            };
            resultado = bsReserva.ModificarDetalleReserva(ed);

            if (resultado != 0)
            {
                MessageBox.Show("Registro modificado con exito");
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error");
            }
            this.detalleReservasTableAdapter.Fill(this.dS_DetalleReservas.DetalleReservas);
        }
    }
}
    

        
