﻿namespace Capa_Presentacion
{
    partial class Form_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);

            this.detalleReservasBindingSource = new System.Windows.Forms.BindingSource(this.components);

            this.reservasBindingSource = new System.Windows.Forms.BindingSource(this.components);

            this.dSClientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.label38 = new System.Windows.Forms.Label();
            this.txtcantidaddr = new System.Windows.Forms.TextBox();
            this.cbproductosdr = new System.Windows.Forms.ComboBox();
            this.productosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label27 = new System.Windows.Forms.Label();
            this.lbncliente = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lbnreserva = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.btn_agregardr = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btactumoddr = new System.Windows.Forms.Button();
            this.cblineamod = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtcantidadmod = new System.Windows.Forms.TextBox();
            this.cbproductomod = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.lblnclientedr = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.lblnreservadr = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.btmoddr = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btBuscarReserva = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.btactumodre = new System.Windows.Forms.Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.IdReserva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodClienter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prioridad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Realizada = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtcodclimodre = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.btcontDR = new System.Windows.Forms.Button();
            this.rbmodre = new System.Windows.Forms.RadioButton();
            this.txtpreciomodre = new System.Windows.Forms.TextBox();
            this.cbmodre = new System.Windows.Forms.CheckedListBox();
            this.dpmodre = new System.Windows.Forms.DateTimePicker();
            this.txtidmodre = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.bt_anadir_reserva = new System.Windows.Forms.Button();
            this.rbsi = new System.Windows.Forms.RadioButton();
            this.txtprecio = new System.Windows.Forms.TextBox();
            this.cbprioridad = new System.Windows.Forms.CheckedListBox();
            this.dtmodfecha = new System.Windows.Forms.DateTimePicker();
            this.txtreservacodcliente = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.idReservaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codClienteDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numlineaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codProductoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.idReservaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codClienteDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prioridadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.realizadaDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbempresa = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.codClienteDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.nombreDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellido1DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellido2DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefonoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.txtcp = new System.Windows.Forms.TextBox();
            this.txtapellido2 = new System.Windows.Forms.TextBox();
            this.txtapellido = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.btanadir = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.bt_Modificar = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCodClienteborrar = new System.Windows.Forms.TextBox();
            this.txttelefonoborrar = new System.Windows.Forms.TextBox();
            this.txtdireccionborrar = new System.Windows.Forms.TextBox();
            this.txtcpborrar = new System.Windows.Forms.TextBox();
            this.txtape2borrar = new System.Windows.Forms.TextBox();
            this.txtape1borrar = new System.Windows.Forms.TextBox();
            this.txtnombreborrar = new System.Windows.Forms.TextBox();
            this.cbtipoborrar = new System.Windows.Forms.CheckBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.CodCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apellido1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apellido2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.bt_borrar = new System.Windows.Forms.Button();
            this.codClienteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellido1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellido2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefonoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_Clientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detalleReservasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_DetalleReservas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reservasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_Reservas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSClientesBindingSource)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_Productos)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "Clientes";
            this.clientesBindingSource.DataSource = this.dS_Clientes;
            // 
            // dS_Clientes
            // 
           
            // 
            // reservasBindingSource
            // 
            this.reservasBindingSource.DataMember = "Reservas";
            this.reservasBindingSource.DataSource = this.dS_Reservas;
            // 
          

            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(190, 41);
            this.label16.TabIndex = 1;
            this.label16.Text = "CLIENTES";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(212, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(468, 41);
            this.label17.TabIndex = 2;
            this.label17.Text = "RESERVAS";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
           
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.label38);
            this.tabPage7.Controls.Add(this.txtcantidaddr);
            this.tabPage7.Controls.Add(this.cbproductosdr);
            this.tabPage7.Controls.Add(this.label27);
            this.tabPage7.Controls.Add(this.lbncliente);
            this.tabPage7.Controls.Add(this.label35);
            this.tabPage7.Controls.Add(this.lbnreserva);
            this.tabPage7.Controls.Add(this.label33);
            this.tabPage7.Controls.Add(this.btn_agregardr);
            this.tabPage7.Controls.Add(this.label32);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1271, 470);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Añadir Detalle Reserva";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(123, 209);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(49, 13);
            this.label38.TabIndex = 33;
            this.label38.Text = "Cantidad";
            // 
            // txtcantidaddr
            // 
            this.txtcantidaddr.Location = new System.Drawing.Point(262, 202);
            this.txtcantidaddr.Name = "txtcantidaddr";
            this.txtcantidaddr.Size = new System.Drawing.Size(100, 20);
            this.txtcantidaddr.TabIndex = 32;
            // 
            // cbproductosdr
            // 
            this.cbproductosdr.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.productosBindingSource, "IdProducto", true));
            this.cbproductosdr.DataSource = this.productosBindingSource;
            this.cbproductosdr.DisplayMember = "Descripccion";
            this.cbproductosdr.FormattingEnabled = true;
            this.cbproductosdr.Location = new System.Drawing.Point(262, 146);
            this.cbproductosdr.Name = "cbproductosdr";
            this.cbproductosdr.Size = new System.Drawing.Size(121, 21);
            this.cbproductosdr.TabIndex = 31;
            this.cbproductosdr.ValueMember = "IdProducto";
¡
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(123, 146);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(86, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "Elige el producto";
            // 
            // lbncliente
            // 
            this.lbncliente.AutoSize = true;
            this.lbncliente.Location = new System.Drawing.Point(474, 78);
            this.lbncliente.Name = "lbncliente";
            this.lbncliente.Size = new System.Drawing.Size(31, 13);
            this.lbncliente.TabIndex = 29;
            this.lbncliente.Text = "vigila";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(374, 78);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(93, 13);
            this.label35.TabIndex = 28;
            this.label35.Text = "del cliente numero";
            // 
            // lbnreserva
            // 
            this.lbnreserva.AutoSize = true;
            this.lbnreserva.Location = new System.Drawing.Point(326, 78);
            this.lbnreserva.Name = "lbnreserva";
            this.lbnreserva.Size = new System.Drawing.Size(31, 13);
            this.lbnreserva.TabIndex = 27;
            this.lbnreserva.Text = "vigila";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(120, 41);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 13);
            this.label33.TabIndex = 26;
            this.label33.Text = "RECUERDA";
            // 
            // btn_agregardr
            // 
            this.btn_agregardr.Location = new System.Drawing.Point(129, 279);
            this.btn_agregardr.Name = "btn_agregardr";
            this.btn_agregardr.Size = new System.Drawing.Size(254, 35);
            this.btn_agregardr.TabIndex = 24;
            this.btn_agregardr.Text = "Agregar";
            this.btn_agregardr.UseVisualStyleBackColor = true;
            this.btn_agregardr.Click += new System.EventHandler(this.btn_agregardr_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(120, 78);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(200, 13);
            this.label32.TabIndex = 14;
            this.label32.Text = "Estas añadiedo informacion a la Reserva";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btactumoddr);
            this.tabPage6.Controls.Add(this.cblineamod);
            this.tabPage6.Controls.Add(this.label47);
            this.tabPage6.Controls.Add(this.label40);
            this.tabPage6.Controls.Add(this.txtcantidadmod);
            this.tabPage6.Controls.Add(this.cbproductomod);
            this.tabPage6.Controls.Add(this.label41);
            this.tabPage6.Controls.Add(this.lblnclientedr);
            this.tabPage6.Controls.Add(this.label43);
            this.tabPage6.Controls.Add(this.lblnreservadr);
            this.tabPage6.Controls.Add(this.label45);
            this.tabPage6.Controls.Add(this.btmoddr);
            this.tabPage6.Controls.Add(this.label46);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1271, 470);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Modificar Detalle Reserva";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btactumoddr
            // 
            this.btactumoddr.Location = new System.Drawing.Point(334, 103);
            this.btactumoddr.Name = "btactumoddr";
            this.btactumoddr.Size = new System.Drawing.Size(118, 27);
            this.btactumoddr.TabIndex = 46;
            this.btactumoddr.Text = "Actualizar campos";
            this.btactumoddr.UseVisualStyleBackColor = true;
            this.btactumoddr.Click += new System.EventHandler(this.btactumoddr_Click);
            // 
            // cblineamod
            // 
            this.cblineamod.DataSource = this.detalleReservasBindingSource;
            this.cblineamod.DisplayMember = "Numlinea";
            this.cblineamod.FormattingEnabled = true;
            this.cblineamod.Location = new System.Drawing.Point(184, 107);
            this.cblineamod.Name = "cblineamod";
            this.cblineamod.Size = new System.Drawing.Size(121, 21);
            this.cblineamod.TabIndex = 45;
            this.cblineamod.ValueMember = "Numlinea";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(45, 110);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(122, 13);
            this.label47.TabIndex = 44;
            this.label47.Text = "Elige el  numero de linea";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(45, 229);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(49, 13);
            this.label40.TabIndex = 43;
            this.label40.Text = "Cantidad";
            // 
            // txtcantidadmod
            // 
            this.txtcantidadmod.Location = new System.Drawing.Point(184, 222);
            this.txtcantidadmod.Name = "txtcantidadmod";
            this.txtcantidadmod.Size = new System.Drawing.Size(100, 20);
            this.txtcantidadmod.TabIndex = 42;
            // 
            // cbproductomod
            // 
            this.cbproductomod.DataSource = this.productosBindingSource;
            this.cbproductomod.DisplayMember = "Descripccion";
            this.cbproductomod.FormattingEnabled = true;
            this.cbproductomod.Location = new System.Drawing.Point(184, 171);
            this.cbproductomod.Name = "cbproductomod";
            this.cbproductomod.Size = new System.Drawing.Size(121, 21);
            this.cbproductomod.TabIndex = 41;
            this.cbproductomod.ValueMember = "IdProducto";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(45, 171);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(86, 13);
            this.label41.TabIndex = 40;
            this.label41.Text = "Elige el producto";
            // 
            // lblnclientedr
            // 
            this.lblnclientedr.AutoSize = true;
            this.lblnclientedr.Location = new System.Drawing.Point(396, 72);
            this.lblnclientedr.Name = "lblnclientedr";
            this.lblnclientedr.Size = new System.Drawing.Size(31, 13);
            this.lblnclientedr.TabIndex = 39;
            this.lblnclientedr.Text = "vigila";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(296, 72);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(93, 13);
            this.label43.TabIndex = 38;
            this.label43.Text = "del cliente numero";
            // 
            // lblnreservadr
            // 
            this.lblnreservadr.AutoSize = true;
            this.lblnreservadr.Location = new System.Drawing.Point(248, 72);
            this.lblnreservadr.Name = "lblnreservadr";
            this.lblnreservadr.Size = new System.Drawing.Size(31, 13);
            this.lblnreservadr.TabIndex = 37;
            this.lblnreservadr.Text = "vigila";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(42, 35);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 13);
            this.label45.TabIndex = 36;
            this.label45.Text = "RECUERDA";
            // 
            // btmoddr
            // 
            this.btmoddr.Location = new System.Drawing.Point(344, 309);
            this.btmoddr.Name = "btmoddr";
            this.btmoddr.Size = new System.Drawing.Size(254, 35);
            this.btmoddr.TabIndex = 35;
            this.btmoddr.Text = "Modificar";
            this.btmoddr.UseVisualStyleBackColor = true;
            this.btmoddr.Click += new System.EventHandler(this.btmoddr_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(42, 72);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(200, 13);
            this.label46.TabIndex = 34;
            this.label46.Text = "Estas añadiedo informacion a la Reserva";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btBuscarReserva);
            this.tabPage5.Controls.Add(this.label39);
            this.tabPage5.Controls.Add(this.btactumodre);
            this.tabPage5.Controls.Add(this.dataGridView5);
            this.tabPage5.Controls.Add(this.txtcodclimodre);
            this.tabPage5.Controls.Add(this.label37);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.btcontDR);
            this.tabPage5.Controls.Add(this.rbmodre);
            this.tabPage5.Controls.Add(this.txtpreciomodre);
            this.tabPage5.Controls.Add(this.cbmodre);
            this.tabPage5.Controls.Add(this.dpmodre);
            this.tabPage5.Controls.Add(this.txtidmodre);
            this.tabPage5.Controls.Add(this.label29);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.label31);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.label36);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1271, 470);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Modificar Reserva";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btBuscarReserva
            // 
            this.btBuscarReserva.Location = new System.Drawing.Point(264, 50);
            this.btBuscarReserva.Name = "btBuscarReserva";
            this.btBuscarReserva.Size = new System.Drawing.Size(110, 27);
            this.btBuscarReserva.TabIndex = 31;
            this.btBuscarReserva.Text = "Buscar";
            this.btBuscarReserva.UseVisualStyleBackColor = true;
            this.btBuscarReserva.Click += new System.EventHandler(this.btBuscarReserva_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(884, 314);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(145, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "*Se actualizara todo despues";
            // 
            // btactumodre
            // 
            this.btactumodre.Location = new System.Drawing.Point(673, 276);
            this.btactumodre.Name = "btactumodre";
            this.btactumodre.Size = new System.Drawing.Size(143, 35);
            this.btactumodre.TabIndex = 29;
            this.btactumodre.Text = "Actualizar";
            this.btactumodre.UseVisualStyleBackColor = true;
            this.btactumodre.Click += new System.EventHandler(this.btactumodre_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToDeleteRows = false;
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView5.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdReserva,
            this.CodClienter,
            this.fecha,
            this.Prioridad,
            this.Precio,
            this.Realizada});
            this.dataGridView5.DataSource = this.reservasBindingSource;
            this.dataGridView5.Location = new System.Drawing.Point(4, 102);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.ReadOnly = true;
            this.dataGridView5.Size = new System.Drawing.Size(644, 150);
            this.dataGridView5.TabIndex = 28;
            this.dataGridView5.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView5_CellContentClick);
            // 
            // IdReserva
            // 
            this.IdReserva.DataPropertyName = "IdReserva";
            this.IdReserva.HeaderText = "IdReserva";
            this.IdReserva.Name = "IdReserva";
            this.IdReserva.ReadOnly = true;
            // 
            // CodClienter
            // 
            this.CodClienter.DataPropertyName = "CodCliente";
            this.CodClienter.HeaderText = "CodCliente";
            this.CodClienter.Name = "CodClienter";
            this.CodClienter.ReadOnly = true;
            // 
            // fecha
            // 
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            // 
            // Prioridad
            // 
            this.Prioridad.DataPropertyName = "Prioridad";
            this.Prioridad.HeaderText = "Prioridad";
            this.Prioridad.Name = "Prioridad";
            this.Prioridad.ReadOnly = true;
            // 
            // Precio
            // 
            this.Precio.DataPropertyName = "Precio";
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            // 
            // Realizada
            // 
            this.Realizada.DataPropertyName = "Realizada";
            this.Realizada.HeaderText = "Realizada";
            this.Realizada.Name = "Realizada";
            this.Realizada.ReadOnly = true;
            // 
            // txtcodclimodre
            // 
            this.txtcodclimodre.Location = new System.Drawing.Point(782, 50);
            this.txtcodclimodre.Name = "txtcodclimodre";
            this.txtcodclimodre.Size = new System.Drawing.Size(100, 20);
            this.txtcodclimodre.TabIndex = 27;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(670, 53);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 26;
            this.label37.Text = "Codigo Cliente";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(838, 235);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(160, 13);
            this.label28.TabIndex = 25;
            this.label28.Text = "Dejar en blanco para pendiente*";
            // 
            // btcontDR
            // 
            this.btcontDR.Location = new System.Drawing.Point(835, 276);
            this.btcontDR.Name = "btcontDR";
            this.btcontDR.Size = new System.Drawing.Size(254, 35);
            this.btcontDR.TabIndex = 24;
            this.btcontDR.Text = "Continuar a Detalle Reserva";
            this.btcontDR.UseVisualStyleBackColor = true;
            this.btcontDR.Click += new System.EventHandler(this.btcontDR_Click);
            // 
            // rbmodre
            // 
            this.rbmodre.AutoSize = true;
            this.rbmodre.Location = new System.Drawing.Point(782, 235);
            this.rbmodre.Name = "rbmodre";
            this.rbmodre.Size = new System.Drawing.Size(34, 17);
            this.rbmodre.TabIndex = 23;
            this.rbmodre.TabStop = true;
            this.rbmodre.Text = "Si";
            this.rbmodre.UseVisualStyleBackColor = true;
            // 
            // txtpreciomodre
            // 
            this.txtpreciomodre.Location = new System.Drawing.Point(782, 189);
            this.txtpreciomodre.Name = "txtpreciomodre";
            this.txtpreciomodre.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtpreciomodre.Size = new System.Drawing.Size(100, 20);
            this.txtpreciomodre.TabIndex = 22;
            this.txtpreciomodre.Text = "€";
            this.txtpreciomodre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbmodre
            // 
            this.cbmodre.FormattingEnabled = true;
            this.cbmodre.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cbmodre.Location = new System.Drawing.Point(782, 139);
            this.cbmodre.Name = "cbmodre";
            this.cbmodre.Size = new System.Drawing.Size(307, 34);
            this.cbmodre.TabIndex = 21;
            // 
            // dpmodre
            // 
            this.dpmodre.Location = new System.Drawing.Point(782, 86);
            this.dpmodre.Name = "dpmodre";
            this.dpmodre.Size = new System.Drawing.Size(223, 20);
            this.dpmodre.TabIndex = 20;
            // 
            // txtidmodre
            // 
            this.txtidmodre.Location = new System.Drawing.Point(148, 54);
            this.txtidmodre.Name = "txtidmodre";
            this.txtidmodre.Size = new System.Drawing.Size(100, 20);
            this.txtidmodre.TabIndex = 19;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(670, 235);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 13);
            this.label29.TabIndex = 18;
            this.label29.Text = "Realizada";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(670, 189);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(37, 13);
            this.label30.TabIndex = 17;
            this.label30.Text = "Precio";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(670, 139);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(48, 13);
            this.label31.TabIndex = 16;
            this.label31.Text = "Prioridad";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(670, 92);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(37, 13);
            this.label34.TabIndex = 15;
            this.label34.Text = "Fecha";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(29, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(113, 13);
            this.label36.TabIndex = 14;
            this.label36.Text = "Id Reserva a modificar";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.bt_anadir_reserva);
            this.tabPage4.Controls.Add(this.rbsi);
            this.tabPage4.Controls.Add(this.txtprecio);
            this.tabPage4.Controls.Add(this.cbprioridad);
            this.tabPage4.Controls.Add(this.dtmodfecha);
            this.tabPage4.Controls.Add(this.txtreservacodcliente);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabPage4.Size = new System.Drawing.Size(1271, 470);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Añadir Reserva";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(545, 250);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(160, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "Dejar en blanco para pendiente*";
            // 
            // bt_anadir_reserva
            // 
            this.bt_anadir_reserva.Location = new System.Drawing.Point(433, 311);
            this.bt_anadir_reserva.Name = "bt_anadir_reserva";
            this.bt_anadir_reserva.Size = new System.Drawing.Size(254, 35);
            this.bt_anadir_reserva.TabIndex = 12;
            this.bt_anadir_reserva.Text = "Continuar a Detalle Reserva";
            this.bt_anadir_reserva.UseVisualStyleBackColor = true;
            this.bt_anadir_reserva.Click += new System.EventHandler(this.bt_anadir_reserva_click);
            // 
            // rbsi
            // 
            this.rbsi.AutoSize = true;
            this.rbsi.Location = new System.Drawing.Point(489, 250);
            this.rbsi.Name = "rbsi";
            this.rbsi.Size = new System.Drawing.Size(34, 17);
            this.rbsi.TabIndex = 10;
            this.rbsi.TabStop = true;
            this.rbsi.Text = "Si";
            this.rbsi.UseVisualStyleBackColor = true;
            // 
            // txtprecio
            // 
            this.txtprecio.Location = new System.Drawing.Point(489, 204);
            this.txtprecio.Name = "txtprecio";
            this.txtprecio.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtprecio.Size = new System.Drawing.Size(100, 20);
            this.txtprecio.TabIndex = 9;
            this.txtprecio.Text = "€";
            this.txtprecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbprioridad
            // 
            this.cbprioridad.FormattingEnabled = true;
            this.cbprioridad.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cbprioridad.Location = new System.Drawing.Point(489, 154);
            this.cbprioridad.Name = "cbprioridad";
            this.cbprioridad.Size = new System.Drawing.Size(307, 34);
            this.cbprioridad.TabIndex = 8;
            this.cbprioridad.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // dtmodfecha
            // 
            this.dtmodfecha.Location = new System.Drawing.Point(489, 114);
            this.dtmodfecha.Name = "dtmodfecha";
            this.dtmodfecha.Size = new System.Drawing.Size(223, 20);
            this.dtmodfecha.TabIndex = 7;
            // 
            // txtreservacodcliente
            // 
            this.txtreservacodcliente.Location = new System.Drawing.Point(489, 74);
            this.txtreservacodcliente.Name = "txtreservacodcliente";
            this.txtreservacodcliente.Size = new System.Drawing.Size(100, 20);
            this.txtreservacodcliente.TabIndex = 6;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(377, 250);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 13);
            this.label26.TabIndex = 5;
            this.label26.Text = "Realizada";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(377, 204);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Precio";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(377, 154);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 3;
            this.label24.Text = "Prioridad";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(377, 114);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(37, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Fecha";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(377, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(90, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Codigo de Cliente";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView4);
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1271, 470);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Listado de Reservas";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idReservaDataGridViewTextBoxColumn1,
            this.codClienteDataGridViewTextBoxColumn2,
            this.numlineaDataGridViewTextBoxColumn,
            this.codProductoDataGridViewTextBoxColumn,
            this.cantidadDataGridViewTextBoxColumn});
            this.dataGridView4.DataSource = this.detalleReservasBindingSource;
            this.dataGridView4.Location = new System.Drawing.Point(706, 0);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(565, 464);
            this.dataGridView4.TabIndex = 1;
            // 
            // idReservaDataGridViewTextBoxColumn1
            // 
            this.idReservaDataGridViewTextBoxColumn1.DataPropertyName = "IdReserva";
            this.idReservaDataGridViewTextBoxColumn1.HeaderText = "IdReserva";
            this.idReservaDataGridViewTextBoxColumn1.Name = "idReservaDataGridViewTextBoxColumn1";
            // 
            // codClienteDataGridViewTextBoxColumn2
            // 
            this.codClienteDataGridViewTextBoxColumn2.DataPropertyName = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn2.HeaderText = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn2.Name = "codClienteDataGridViewTextBoxColumn2";
            // 
            // numlineaDataGridViewTextBoxColumn
            // 
            this.numlineaDataGridViewTextBoxColumn.DataPropertyName = "Numlinea";
            this.numlineaDataGridViewTextBoxColumn.HeaderText = "Numlinea";
            this.numlineaDataGridViewTextBoxColumn.Name = "numlineaDataGridViewTextBoxColumn";
            // 
            // codProductoDataGridViewTextBoxColumn
            // 
            this.codProductoDataGridViewTextBoxColumn.DataPropertyName = "CodProducto";
            this.codProductoDataGridViewTextBoxColumn.HeaderText = "CodProducto";
            this.codProductoDataGridViewTextBoxColumn.Name = "codProductoDataGridViewTextBoxColumn";
            // 
            // cantidadDataGridViewTextBoxColumn
            // 
            this.cantidadDataGridViewTextBoxColumn.DataPropertyName = "Cantidad";
            this.cantidadDataGridViewTextBoxColumn.HeaderText = "Cantidad";
            this.cantidadDataGridViewTextBoxColumn.Name = "cantidadDataGridViewTextBoxColumn";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idReservaDataGridViewTextBoxColumn,
            this.codClienteDataGridViewTextBoxColumn1,
            this.fechaDataGridViewTextBoxColumn,
            this.prioridadDataGridViewTextBoxColumn,
            this.precioDataGridViewTextBoxColumn,
            this.realizadaDataGridViewCheckBoxColumn});
            this.dataGridView3.DataSource = this.reservasBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(700, 465);
            this.dataGridView3.TabIndex = 0;
            // 
            // idReservaDataGridViewTextBoxColumn
            // 
            this.idReservaDataGridViewTextBoxColumn.DataPropertyName = "IdReserva";
            this.idReservaDataGridViewTextBoxColumn.HeaderText = "IdReserva";
            this.idReservaDataGridViewTextBoxColumn.Name = "idReservaDataGridViewTextBoxColumn";
            // 
            // codClienteDataGridViewTextBoxColumn1
            // 
            this.codClienteDataGridViewTextBoxColumn1.DataPropertyName = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn1.HeaderText = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn1.Name = "codClienteDataGridViewTextBoxColumn1";
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "fecha";
            this.fechaDataGridViewTextBoxColumn.HeaderText = "fecha";
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            // 
            // prioridadDataGridViewTextBoxColumn
            // 
            this.prioridadDataGridViewTextBoxColumn.DataPropertyName = "Prioridad";
            this.prioridadDataGridViewTextBoxColumn.HeaderText = "Prioridad";
            this.prioridadDataGridViewTextBoxColumn.Name = "prioridadDataGridViewTextBoxColumn";
            // 
            // precioDataGridViewTextBoxColumn
            // 
            this.precioDataGridViewTextBoxColumn.DataPropertyName = "Precio";
            this.precioDataGridViewTextBoxColumn.HeaderText = "Precio";
            this.precioDataGridViewTextBoxColumn.Name = "precioDataGridViewTextBoxColumn";
            // 
            // realizadaDataGridViewCheckBoxColumn
            // 
            this.realizadaDataGridViewCheckBoxColumn.DataPropertyName = "Realizada";
            this.realizadaDataGridViewCheckBoxColumn.HeaderText = "Realizada";
            this.realizadaDataGridViewCheckBoxColumn.Name = "realizadaDataGridViewCheckBoxColumn";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(12, 53);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1279, 496);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.cbempresa);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.txttelefono);
            this.tabPage1.Controls.Add(this.txtdireccion);
            this.tabPage1.Controls.Add(this.txtcp);
            this.tabPage1.Controls.Add(this.txtapellido2);
            this.tabPage1.Controls.Add(this.txtapellido);
            this.tabPage1.Controls.Add(this.txtnombre);
            this.tabPage1.Controls.Add(this.btanadir);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1271, 470);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Añadir Cliente";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Telefono";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Direccion";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Apellido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "CP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Tipo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Nombre del cliente";
            // 
            // cbempresa
            // 
            this.cbempresa.AutoSize = true;
            this.cbempresa.Location = new System.Drawing.Point(111, 73);
            this.cbempresa.Name = "cbempresa";
            this.cbempresa.Size = new System.Drawing.Size(67, 17);
            this.cbempresa.TabIndex = 16;
            this.cbempresa.Text = "Empresa";
            this.cbempresa.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codClienteDataGridViewTextBoxColumn3,
            this.tipoDataGridViewCheckBoxColumn1,
            this.nombreDataGridViewTextBoxColumn1,
            this.apellido1DataGridViewTextBoxColumn1,
            this.apellido2DataGridViewTextBoxColumn1,
            this.cPDataGridViewTextBoxColumn1,
            this.direccionDataGridViewTextBoxColumn1,
            this.telefonoDataGridViewTextBoxColumn1});
            this.dataGridView1.DataSource = this.clientesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(258, 47);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridView1.Size = new System.Drawing.Size(863, 288);
            this.dataGridView1.TabIndex = 15;
            // 
            // codClienteDataGridViewTextBoxColumn3
            // 
            this.codClienteDataGridViewTextBoxColumn3.DataPropertyName = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn3.HeaderText = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn3.Name = "codClienteDataGridViewTextBoxColumn3";
            // 
            // tipoDataGridViewCheckBoxColumn1
            // 
            this.tipoDataGridViewCheckBoxColumn1.DataPropertyName = "Tipo";
            this.tipoDataGridViewCheckBoxColumn1.HeaderText = "Tipo";
            this.tipoDataGridViewCheckBoxColumn1.Name = "tipoDataGridViewCheckBoxColumn1";
            // 
            // nombreDataGridViewTextBoxColumn1
            // 
            this.nombreDataGridViewTextBoxColumn1.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.Name = "nombreDataGridViewTextBoxColumn1";
            // 
            // apellido1DataGridViewTextBoxColumn1
            // 
            this.apellido1DataGridViewTextBoxColumn1.DataPropertyName = "Apellido1";
            this.apellido1DataGridViewTextBoxColumn1.HeaderText = "Apellido1";
            this.apellido1DataGridViewTextBoxColumn1.Name = "apellido1DataGridViewTextBoxColumn1";
            // 
            // apellido2DataGridViewTextBoxColumn1
            // 
            this.apellido2DataGridViewTextBoxColumn1.DataPropertyName = "Apellido2";
            this.apellido2DataGridViewTextBoxColumn1.HeaderText = "Apellido2";
            this.apellido2DataGridViewTextBoxColumn1.Name = "apellido2DataGridViewTextBoxColumn1";
            // 
            // cPDataGridViewTextBoxColumn1
            // 
            this.cPDataGridViewTextBoxColumn1.DataPropertyName = "CP";
            this.cPDataGridViewTextBoxColumn1.HeaderText = "CP";
            this.cPDataGridViewTextBoxColumn1.Name = "cPDataGridViewTextBoxColumn1";
            // 
            // direccionDataGridViewTextBoxColumn1
            // 
            this.direccionDataGridViewTextBoxColumn1.DataPropertyName = "Direccion";
            this.direccionDataGridViewTextBoxColumn1.HeaderText = "Direccion";
            this.direccionDataGridViewTextBoxColumn1.Name = "direccionDataGridViewTextBoxColumn1";
            // 
            // telefonoDataGridViewTextBoxColumn1
            // 
            this.telefonoDataGridViewTextBoxColumn1.DataPropertyName = "Telefono";
            this.telefonoDataGridViewTextBoxColumn1.HeaderText = "Telefono";
            this.telefonoDataGridViewTextBoxColumn1.Name = "telefonoDataGridViewTextBoxColumn1";
            // 
            // txttelefono
            // 
            this.txttelefono.Location = new System.Drawing.Point(111, 203);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(100, 20);
            this.txttelefono.TabIndex = 12;
            // 
            // txtdireccion
            // 
            this.txtdireccion.Location = new System.Drawing.Point(111, 177);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(100, 20);
            this.txtdireccion.TabIndex = 11;
            // 
            // txtcp
            // 
            this.txtcp.Location = new System.Drawing.Point(111, 151);
            this.txtcp.Name = "txtcp";
            this.txtcp.Size = new System.Drawing.Size(100, 20);
            this.txtcp.TabIndex = 5;
            // 
            // txtapellido2
            // 
            this.txtapellido2.Location = new System.Drawing.Point(111, 125);
            this.txtapellido2.Name = "txtapellido2";
            this.txtapellido2.Size = new System.Drawing.Size(100, 20);
            this.txtapellido2.TabIndex = 4;
            // 
            // txtapellido
            // 
            this.txtapellido.Location = new System.Drawing.Point(111, 99);
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(100, 20);
            this.txtapellido.TabIndex = 3;
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(111, 47);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(100, 20);
            this.txtnombre.TabIndex = 1;
            // 
            // btanadir
            // 
            this.btanadir.Location = new System.Drawing.Point(42, 258);
            this.btanadir.Name = "btanadir";
            this.btanadir.Size = new System.Drawing.Size(75, 23);
            this.btanadir.TabIndex = 0;
            this.btanadir.Text = "Añadir";
            this.btanadir.UseVisualStyleBackColor = true;
            this.btanadir.Click += new System.EventHandler(this.btanadir_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.bt_Modificar);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.txtCodClienteborrar);
            this.tabPage2.Controls.Add(this.txttelefonoborrar);
            this.tabPage2.Controls.Add(this.txtdireccionborrar);
            this.tabPage2.Controls.Add(this.txtcpborrar);
            this.tabPage2.Controls.Add(this.txtape2borrar);
            this.tabPage2.Controls.Add(this.txtape1borrar);
            this.tabPage2.Controls.Add(this.txtnombreborrar);
            this.tabPage2.Controls.Add(this.cbtipoborrar);
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.bt_borrar);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1271, 470);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modificar/Borrar Cliente";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(20, 70);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "CodCliente";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Telefono";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 223);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Direccion";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 175);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "Apellido";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 201);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 39;
            this.label12.Text = "CP";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "Nombre";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 37;
            this.label18.Text = "Tipo";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Nombre del cliente";
            // 
            // bt_Modificar
            // 
            this.bt_Modificar.Location = new System.Drawing.Point(37, 301);
            this.bt_Modificar.Name = "bt_Modificar";
            this.bt_Modificar.Size = new System.Drawing.Size(75, 23);
            this.bt_Modificar.TabIndex = 35;
            this.bt_Modificar.Text = "Modificar";
            this.bt_Modificar.UseVisualStyleBackColor = true;
            this.bt_Modificar.Click += new System.EventHandler(this.Bt_Modificar_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(34, -25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 34;
            // 
            // txtCodClienteborrar
            // 
            this.txtCodClienteborrar.Location = new System.Drawing.Point(132, 63);
            this.txtCodClienteborrar.Name = "txtCodClienteborrar";
            this.txtCodClienteborrar.Size = new System.Drawing.Size(100, 20);
            this.txtCodClienteborrar.TabIndex = 33;
            // 
            // txttelefonoborrar
            // 
            this.txttelefonoborrar.Location = new System.Drawing.Point(132, 249);
            this.txttelefonoborrar.Name = "txttelefonoborrar";
            this.txttelefonoborrar.Size = new System.Drawing.Size(100, 20);
            this.txttelefonoborrar.TabIndex = 28;
            // 
            // txtdireccionborrar
            // 
            this.txtdireccionborrar.Location = new System.Drawing.Point(132, 223);
            this.txtdireccionborrar.Name = "txtdireccionborrar";
            this.txtdireccionborrar.Size = new System.Drawing.Size(100, 20);
            this.txtdireccionborrar.TabIndex = 27;
            // 
            // txtcpborrar
            // 
            this.txtcpborrar.Location = new System.Drawing.Point(132, 197);
            this.txtcpborrar.Name = "txtcpborrar";
            this.txtcpborrar.Size = new System.Drawing.Size(100, 20);
            this.txtcpborrar.TabIndex = 21;
            // 
            // txtape2borrar
            // 
            this.txtape2borrar.Location = new System.Drawing.Point(132, 171);
            this.txtape2borrar.Name = "txtape2borrar";
            this.txtape2borrar.Size = new System.Drawing.Size(100, 20);
            this.txtape2borrar.TabIndex = 20;
            // 
            // txtape1borrar
            // 
            this.txtape1borrar.Location = new System.Drawing.Point(132, 145);
            this.txtape1borrar.Name = "txtape1borrar";
            this.txtape1borrar.Size = new System.Drawing.Size(100, 20);
            this.txtape1borrar.TabIndex = 19;
            // 
            // txtnombreborrar
            // 
            this.txtnombreborrar.Location = new System.Drawing.Point(132, 93);
            this.txtnombreborrar.Name = "txtnombreborrar";
            this.txtnombreborrar.Size = new System.Drawing.Size(100, 20);
            this.txtnombreborrar.TabIndex = 18;
            // 
            // cbtipoborrar
            // 
            this.cbtipoborrar.AutoSize = true;
            this.cbtipoborrar.Location = new System.Drawing.Point(132, 119);
            this.cbtipoborrar.Name = "cbtipoborrar";
            this.cbtipoborrar.Size = new System.Drawing.Size(67, 17);
            this.cbtipoborrar.TabIndex = 32;
            this.cbtipoborrar.Text = "Empresa";
            this.cbtipoborrar.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodCliente,
            this.Tipo,
            this.Nombre,
            this.Apellido1,
            this.Apellido2,
            this.CP,
            this.Direccion,
            this.Telefono});
            this.dataGridView2.DataSource = this.clientesBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(279, 63);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridView2.Size = new System.Drawing.Size(863, 288);
            this.dataGridView2.TabIndex = 31;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // CodCliente
            // 
            this.CodCliente.DataPropertyName = "CodCliente";
            this.CodCliente.HeaderText = "CodCliente";
            this.CodCliente.Name = "CodCliente";
            // 
            // Tipo
            // 
            this.Tipo.DataPropertyName = "Tipo";
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            // 
            // Nombre
            // 
            this.Nombre.DataPropertyName = "Nombre";
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            // 
            // Apellido1
            // 
            this.Apellido1.DataPropertyName = "Apellido1";
            this.Apellido1.HeaderText = "Apellido1";
            this.Apellido1.Name = "Apellido1";
            // 
            // Apellido2
            // 
            this.Apellido2.DataPropertyName = "Apellido2";
            this.Apellido2.HeaderText = "Apellido2";
            this.Apellido2.Name = "Apellido2";
            // 
            // CP
            // 
            this.CP.DataPropertyName = "CP";
            this.CP.HeaderText = "CP";
            this.CP.Name = "CP";
            // 
            // Direccion
            // 
            this.Direccion.DataPropertyName = "Direccion";
            this.Direccion.HeaderText = "Direccion";
            this.Direccion.Name = "Direccion";
            // 
            // Telefono
            // 
            this.Telefono.DataPropertyName = "Telefono";
            this.Telefono.HeaderText = "Telefono";
            this.Telefono.Name = "Telefono";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(34, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(34, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 22;
            // 
            // bt_borrar
            // 
            this.bt_borrar.Location = new System.Drawing.Point(144, 301);
            this.bt_borrar.Name = "bt_borrar";
            this.bt_borrar.Size = new System.Drawing.Size(75, 23);
            this.bt_borrar.TabIndex = 17;
            this.bt_borrar.Text = "Borrar";
            this.bt_borrar.UseVisualStyleBackColor = true;
            this.bt_borrar.Click += new System.EventHandler(this.bt_borrar_Click);
            // 
            // codClienteDataGridViewTextBoxColumn
            // 
            this.codClienteDataGridViewTextBoxColumn.DataPropertyName = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn.HeaderText = "CodCliente";
            this.codClienteDataGridViewTextBoxColumn.Name = "codClienteDataGridViewTextBoxColumn";
            this.codClienteDataGridViewTextBoxColumn.Width = 103;
            // 
            // tipoDataGridViewCheckBoxColumn
            // 
            this.tipoDataGridViewCheckBoxColumn.DataPropertyName = "Tipo";
            this.tipoDataGridViewCheckBoxColumn.HeaderText = "Tipo";
            this.tipoDataGridViewCheckBoxColumn.Name = "tipoDataGridViewCheckBoxColumn";
            this.tipoDataGridViewCheckBoxColumn.Width = 102;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            this.nombreDataGridViewTextBoxColumn.Width = 103;
            // 
            // apellido1DataGridViewTextBoxColumn
            // 
            this.apellido1DataGridViewTextBoxColumn.DataPropertyName = "Apellido1";
            this.apellido1DataGridViewTextBoxColumn.HeaderText = "Apellido1";
            this.apellido1DataGridViewTextBoxColumn.Name = "apellido1DataGridViewTextBoxColumn";
            this.apellido1DataGridViewTextBoxColumn.Width = 102;
            // 
            // apellido2DataGridViewTextBoxColumn
            // 
            this.apellido2DataGridViewTextBoxColumn.DataPropertyName = "Apellido2";
            this.apellido2DataGridViewTextBoxColumn.HeaderText = "Apellido2";
            this.apellido2DataGridViewTextBoxColumn.Name = "apellido2DataGridViewTextBoxColumn";
            this.apellido2DataGridViewTextBoxColumn.Width = 103;
            // 
            // cPDataGridViewTextBoxColumn
            // 
            this.cPDataGridViewTextBoxColumn.DataPropertyName = "CP";
            this.cPDataGridViewTextBoxColumn.HeaderText = "CP";
            this.cPDataGridViewTextBoxColumn.Name = "cPDataGridViewTextBoxColumn";
            this.cPDataGridViewTextBoxColumn.Width = 102;
            // 
            // direccionDataGridViewTextBoxColumn
            // 
            this.direccionDataGridViewTextBoxColumn.DataPropertyName = "Direccion";
            this.direccionDataGridViewTextBoxColumn.HeaderText = "Direccion";
            this.direccionDataGridViewTextBoxColumn.Name = "direccionDataGridViewTextBoxColumn";
            this.direccionDataGridViewTextBoxColumn.Width = 103;
            // 
            // telefonoDataGridViewTextBoxColumn
            // 
            this.telefonoDataGridViewTextBoxColumn.DataPropertyName = "Telefono";
            this.telefonoDataGridViewTextBoxColumn.HeaderText = "Telefono";
            this.telefonoDataGridViewTextBoxColumn.Name = "telefonoDataGridViewTextBoxColumn";
            this.telefonoDataGridViewTextBoxColumn.Width = 102;
            // 
            // productosTableAdapter
            // 
      
            // Form_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1293, 552);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form_Principal";
            this.Text = "Form_Principal";
            this.Load += new System.EventHandler(this.Form_Principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_Clientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detalleReservasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_DetalleReservas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reservasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_Reservas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSClientesBindingSource)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS_Productos)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DS_Clientes dS_Clientes;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private DS_ClientesTableAdapters.ClientesTableAdapter clientesTableAdapter;
        private System.Windows.Forms.BindingSource dSClientesBindingSource;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private DS_Reservas dS_Reservas;
        private System.Windows.Forms.BindingSource reservasBindingSource;
        private DS_ReservasTableAdapters.ReservasTableAdapter reservasTableAdapter;
        private DS_DetalleReservas dS_DetalleReservas;
        private System.Windows.Forms.BindingSource detalleReservasBindingSource;
        private DS_DetalleReservasTableAdapters.DetalleReservasTableAdapter detalleReservasTableAdapter;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn idReservaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codClienteDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn numlineaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codProductoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idReservaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codClienteDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prioridadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn precioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn realizadaDataGridViewCheckBoxColumn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox cbempresa;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.TextBox txtcp;
        private System.Windows.Forms.TextBox txtapellido2;
        private System.Windows.Forms.TextBox txtapellido;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Button btanadir;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button bt_Modificar;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCodClienteborrar;
        private System.Windows.Forms.TextBox txttelefonoborrar;
        private System.Windows.Forms.TextBox txtdireccionborrar;
        private System.Windows.Forms.TextBox txtcpborrar;
        private System.Windows.Forms.TextBox txtape2borrar;
        private System.Windows.Forms.TextBox txtape1borrar;
        private System.Windows.Forms.TextBox txtnombreborrar;
        private System.Windows.Forms.CheckBox cbtipoborrar;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodCliente;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apellido1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apellido2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefono;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button bt_borrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn codClienteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn tipoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellido1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellido2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefonoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckedListBox cbprioridad;
        private System.Windows.Forms.DateTimePicker dtmodfecha;
        private System.Windows.Forms.TextBox txtreservacodcliente;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtprecio;
        private System.Windows.Forms.Button bt_anadir_reserva;
        private System.Windows.Forms.RadioButton rbsi;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btn_agregardr;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cbproductosdr;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lbncliente;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lbnreserva;
        private System.Windows.Forms.Label label33;
        private DS_Productos dS_Productos;
        private System.Windows.Forms.BindingSource productosBindingSource;
        private DS_ProductosTableAdapters.ProductosTableAdapter productosTableAdapter;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtcantidaddr;
        private System.Windows.Forms.Button btactumoddr;
        private System.Windows.Forms.ComboBox cblineamod;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtcantidadmod;
        private System.Windows.Forms.ComboBox cbproductomod;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label lblnclientedr;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label lblnreservadr;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btmoddr;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button btBuscarReserva;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btactumodre;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.TextBox txtcodclimodre;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btcontDR;
        private System.Windows.Forms.RadioButton rbmodre;
        private System.Windows.Forms.TextBox txtpreciomodre;
        private System.Windows.Forms.CheckedListBox cbmodre;
        private System.Windows.Forms.DateTimePicker dpmodre;
        private System.Windows.Forms.TextBox txtidmodre;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codClienteDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn tipoDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellido1DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellido2DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefonoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdReserva;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodClienter;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prioridad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Realizada;
    }
}